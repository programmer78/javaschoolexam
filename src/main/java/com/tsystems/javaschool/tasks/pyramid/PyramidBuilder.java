package com.tsystems.javaschool.tasks.pyramid;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) {
        try {
            int numberInPyramidBase = 0; //digits in base of the pyramid. It equals number of pyramid levels

            //validate ability of creation Pyramid
            int i = 0;
            do {
                numberInPyramidBase++;
                i += numberInPyramidBase;
            } while (i < inputNumbers.size());

            if (i != inputNumbers.size()) {
                throw new CannotBuildPyramidException();
            }


            //creation pyramid
            //sort the list
            Collections.sort(inputNumbers, new Comparator<Integer>() {
                public int compare(Integer o1, Integer o2) {
                    return o1 - o2;
                }
            });

            int numberInInputList = 0;//number in already sorted input list

            int[][] resultArray = new int[numberInPyramidBase][2 * numberInPyramidBase - 1];
            int x = 0;
            int y = 0;

            try {
                for (int a = numberInPyramidBase + 1; a >= 0; a--) {
                    int b;
                    for (b = 0; b < a; b++) {
                        x++;
                    }

                    for (int j = b; j < numberInPyramidBase + 1; j++) {
                        x++;
                        resultArray[y - 1][x - 2] = inputNumbers.get(numberInInputList);
                        x++;
                        numberInInputList++;
                    }

                    x = 0;
                    y++;
                }
            } catch (ArrayIndexOutOfBoundsException e) {
            }//Exception Driving isn't valid, but I obligatorily redo it if there will be more time.
            
			return resultArray;
        } catch (Throwable throwable) {
            throw new CannotBuildPyramidException();
        }
    }
}
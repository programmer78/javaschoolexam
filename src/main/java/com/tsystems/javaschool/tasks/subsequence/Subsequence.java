package com.tsystems.javaschool.tasks.subsequence;

import java.util.List;

public class Subsequence {

    /**
     * Checks if it is possible to get a sequence which is equal to the first
     * one by removing some elements from the second one.
     *
     * @param x first sequence
     * @param y second sequence
     * @return <code>true</code> if possible, otherwise <code>false</code>
     */
    @SuppressWarnings("rawtypes")
    public boolean find(List x, List y) {

        if (x == null || y == null)
            throw new IllegalArgumentException();

        int counter = 0; //variable check List x
        int marker = 0; //variable show number of element in List y since last cycle
        for (int i = 0; i < x.size(); i++) { //outer cycle. Get elements from List x.
            if (counter < i) { //if counter isn't alike i, List y ends
                break;
            }
            else {
                for (int j = marker; j < y.size(); j++) { //start cycle with j from marker
                    if (x.get(i).equals(y.get(j))) { //if elements are equals move list(increment counter and marker)
                        counter++;
                        marker = j++;
                        break;
                    }
                }
            }
        }
        if (x.size() == counter) {
            return true;
        }
        else {
            return false;
        }
    }
}


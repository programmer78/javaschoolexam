package com.tsystems.javaschool.tasks.calculator;

import java.text.DecimalFormat;
import java.util.LinkedList;
import java.util.NoSuchElementException;

public class Calculator {

    /**
     * Evaluate statement represented as string.
     *
     *mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */
    static boolean isDelim(char c) {//For space symbol
        return c == ' ';
    }

    static boolean isOperator(char c){//For operator symbol
        return c == '+' || c == '-' || c == '*' || c == '/';
    }

    static int priority(char operator){//For getting priority of operations
        switch (operator){
            case '+':
            case '-':
                return 1;
            case '*':
            case '/':
                return 2;
            default:
                return -1;
        }
    }

    static void processOperator(LinkedList<Double> st, char op) {
        double firstNumber = st.removeLast(); //get first
        double secondNumber = st.removeLast(); //and second operands for implementation operations
        switch (op){
            case '+':
                st.add(secondNumber + firstNumber);
                break;
            case '-':
                st.add(secondNumber - firstNumber);
                break;
            case '*':
                st.add(secondNumber * firstNumber);
                break;
            case '/':
                st.add(secondNumber / firstNumber);
                break;
        }
    }

    public String evaluate(String statement) {
        LinkedList<Double> st = new LinkedList<Double>(); //create LinkedList for operands
        LinkedList<Character> op = new LinkedList<Character>(); //the same for operations

        if (statement == null || statement.isEmpty())
            return null;



        for (int i = 0; i < statement.length(); i++) {
            char c = statement.charAt(i);
            if (isDelim(c))  {//miss space symbol
                continue;
            }
            if (c == '(') {//add open parenthesis in operands list
                op.add('(');
            }
            else {
                if (c == ')') {//for close parenthesis try to find open parenthesis
                    try {
                        while (op.getLast() != '(') {
                            processOperator(st,op.removeLast()); //implement operation
                        }
                        op.removeLast(); //remove open parenthesis from the list
                    }catch (NoSuchElementException e) {
                        return null;
                    }
                }
                else {
                    if (isOperator(c)) {//for operator symbol define priority of operation
                        try{
                            while (!op.isEmpty() && priority(op.getLast()) >= priority(c)) {//работаем с приоритетами
                                processOperator(st, op.removeLast());
                            }
                            op.add(c);//add new operation in the op List
                        }
                        catch (NoSuchElementException e){//for duplicated operations
                            return null;
                        }
                    }
                    else {//for digit symbol
                        String operand = "";
                        while (i < statement.length() && (Character.isDigit(statement.charAt(i)) || statement.charAt(i) == '.')) {
                            operand += statement.charAt(i++);
                        }
                        --i;
                        try {
                            st.add(Double.parseDouble(operand));//add number parse string to double type
                        }
                        catch (NumberFormatException e) {//for unknown symbol
                            return null;
                        }
                    }
                }
            }
        }
        try {
            while (!op.isEmpty()) {//implement all operations
                processOperator(st, op.removeLast());
            }
        }
        catch (NoSuchElementException e) { //problems with open parenthesis
            return null;
        }
        catch (ArithmeticException e) { //division by zero exception
            return null;
        }
        DecimalFormat df = new DecimalFormat("##.####"); //format numbers to DecimalFormat
        String result = df.format(st.get(0)).replace(",", ".");
        
        try {
            Double.parseDouble(result);
        } catch (NumberFormatException e){
            return null;
        }
        
        return result; //round and return results
    }

}
